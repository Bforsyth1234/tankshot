import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

interface Tag {
  body: string;
}

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss'],
})

export class TagsComponent {
  @Input() editMode;
  @Input() tags: Tag[] = [];
  @Output() tagsEmitter = new EventEmitter();

  public tagForm = new FormGroup ({
    tags: new FormControl('')
  });

  constructor() {}


  saveTags() {
    const tagsStrings = this.separateString(this.tagForm.value.tags);
    this.mapTags(tagsStrings);
    this.tagsEmitter.emit(this.tags);
    this.tagForm.setValue({tags: ''});
  }

  mapTags(tagStrings: string[]) {
    tagStrings.map(tag => {
      if (tag !== '') {
        this.tags.push({body: tag});
      }
    });
  }

  separateString(tagString: string) {
    return tagString.split(' ');
  }

  editClicked() {
    this.editMode = !this.editMode;
  }
}
