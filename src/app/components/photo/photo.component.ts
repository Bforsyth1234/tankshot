import { Component, Input, OnInit } from '@angular/core';
import { PhotoState } from 'src/app/services/photo/photo.state';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';
import { SavePhoto, GetPhotos } from '../../services/photo/photo.actions';
import { SafeResourceUrl } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-photo-component',
    templateUrl: 'photo.component.html'
})
export class PhotoComponent implements OnInit {
    image: SafeResourceUrl;
    photos$: Observable<any>;
    urlBase = environment.firebase.storageBucket;
    tags: any[];

    constructor(
        private store: Store,
        ) {
        this.store.dispatch(new GetPhotos()).subscribe(() =>
            this.photos$ = this.store.select(PhotoState.getPhotos)
        );
        }

    ngOnInit() {
    }

    onPictureTaken(event) {
        this.image = event;
    }

    savePhoto() {
        console.log('this.setImageObj() = ');
        console.log(this.setImageObj());
        this.store.dispatch([new SavePhoto( this.setImageObj() )]);
    }

    setImageObj() {
        return {
            image: this.image,
            tags: this.tags
        };
    }

    onTagChange(event) {
        console.log('event = ');
        console.log(event);
        this.tags = event;
        console.log('this.tags = ');
        console.log(this.tags);
        // this.store.dispatch(new )
    }
}
