import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { TagsComponent } from './tags/tags.component';
import { CameraComponent } from './camera/camera.component';
import { PhotoComponent } from './photo/photo.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    CameraComponent,
    PhotoComponent,
    TagsComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
  ],
  exports: [
    CameraComponent,
    PhotoComponent,
    TagsComponent,
  ],
  entryComponents: [
    CameraComponent,
    PhotoComponent,
    TagsComponent,
  ],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class ComponentsModule {}
