import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Subscription, Observable } from 'rxjs';
import { SafeResourceUrl } from '@angular/platform-browser';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';
import { PhotoStateModel } from './photo.model';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {
  userDoc: AngularFirestoreDocument<any>;
  user: Observable<any>;
  downloadUrl: Observable<string>;
  uid: String;

  constructor(
    private afs: AngularFirestore,
    public afAuth: AngularFireAuth,
    private storage: AngularFireStorage,
    ) {
      this.afAuth.user.subscribe(data => {
        if (data) {
          this.uid = data.uid;
          this.userDoc = this.afs.doc('users/' + data.uid);
          this.user = this.userDoc.valueChanges();
          this.getPhotos();
        }
      });
    }

    public savePhoto(param) {
      const metadata = {
        contentType: 'image/jpeg',
      };
      const contentType = 'base64';

      param.image.base64 = param.image.base64.replace('data:image/jpeg;base64,', '');
      const file = param.image.base64;
      const filePath = this.uid.toString() + Math.floor((Math.random() * 100) + 1);
      const ref = this.storage.ref(filePath);
      const task = ref.putString(file, contentType, metadata);
      return task.then(item => {
        item.ref.getDownloadURL().then(data => {
          this.savePhotoData(data, param.tags);
        });
      });
    }

  public savePhotoData(url: string, tags): Promise<any> {
    return this.userDoc.collection('pics').add({url: url, tags: tags, uid: this.uid});
  }

  public getPhotos() {
    return this.userDoc.collection<PhotoStateModel>('pics').valueChanges();
  }
}
