import { State, Action, StateContext, Selector } from '@ngxs/store';
import { GetPhotos, SavePhoto } from './photo.actions';
import { PhotoStateModel } from './photo.model';
import { PhotoService } from 'src/app/services/photo/photo.service';


@State<any>({
    name: 'photo',
})
export class PhotoState {

    @Selector()
    static getPhotos(state: PhotoStateModel[]) {
        return state;
    }

    constructor(
        private photoService: PhotoService,
    ) {}

    @Action(GetPhotos)
    getPhotos(
        { setState }: StateContext<PhotoStateModel[]>,
    ) {
        this.photoService.getPhotos().subscribe(data => {
            console.log('data = ');
            console.log(data);
            setState(data);
        });
    }

    @Action(SavePhoto)
    post(
        { patchState }: StateContext<PhotoStateModel>,
        { imgObj }: SavePhoto,
    ) {
        this.photoService.savePhoto(imgObj).then(
            patchState(imgObj)
        );
    }

}
